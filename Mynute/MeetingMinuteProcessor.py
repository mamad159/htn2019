import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim.summarization import summarize

import sys, json

text = sys.argv[1]

print(summarize(text))

sys.stdout.flush()
