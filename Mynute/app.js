const express = require('express');
const assert = require('assert');
const app = express();
const https = require('https');
var feed;

var os = require("fs");

https.get('https://mynute-cc031.firebaseio.com/Voice.json', (resp) => {

    let data = '';

    resp.on('data',(chunk) => {
        data += chunk;
    });

    resp.on('end', ()=>{
        console.log('Here is my data: ' + data);
        feed = JSON.parse(data);
        console.log("I am getting a valid JSON object w/ : " + feed.fields.discussionPOne);

        var meetingTranscription = feed.fields.discussionPOne;

        const spawn = require("child_process").spawn;
        const scriptExecution = spawn("python", ["./MeetingMinuteProcessor.py", meetingTranscription]);
      
        scriptExecution.stdout.on('data', (data) => {
            console.log('Here is my cleaned data: ' + data);

            os.writeFile("latestMinutes.txt", data, (err) => {
                if (err) console.log(error);
                console.log("Your meeting minutes have been successfully written to the file latestMinutes.txt");
            })
        });

        scriptExecution.stderr.on('data', (data) => {
            console.log('stderr: ' + data);
        }
        );

      
        // Write data (remember to send only strings or numbers, otherwhise python wont understand)
        scriptExecution.stdin.write(feed.fields.discussionPOne);
        // End data write
        scriptExecution.stdin.end();

    });

}).on("error", (err) => {
    console.log("Error: " + err.message);
  });

